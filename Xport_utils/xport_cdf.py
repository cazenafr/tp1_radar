
import datetime
import getpass
import platform
import warnings

import netCDF4
import numpy as np

import pyart

#from ..config import FileMetadata
#from .common import stringarray_to_chararray, _test_arguments
#from ..core.radar import Radar
#from ..lazydict import LazyLoadDict

from pyart.core.radar import Radar

def read_xport(filename, field_names=None, additional_metadata=None,
                  file_field_names=False, exclude_fields=None,
                  include_fields=None, delay_field_loading=False, **kwargs):


    # open the file
    dataset = netCDF4.Dataset(filename)
    ncattrs = dataset.ncattrs()
    ncvars = dataset.variables
    

    lat = getattr(dataset, 'RADAR_LAT')
    latitude = dict()
    latitude['data'] = np.array([lat], dtype='float64')
    longitude = dict()
    lon = getattr(dataset, 'RADAR_LON')
    longitude['data'] = np.array([lon], dtype='float64')
    
    altitude = dict()
    altitude['data'] = np.array([0], dtype='float64')
    
    scan_type = getattr(dataset, 'SCAN').lower()

    var = ncvars['Time']
    time = dict((k, getattr(var, k)) for k in var.ncattrs())
    time['data'] = var[:]
    
    var = ncvars['Range']
    _range = dict((k, getattr(var, k)) for k in var.ncattrs())
    _range['data'] = var[:]
    

    var = ncvars['Azimut']
    azimuth = dict((k, getattr(var, k)) for k in var.ncattrs())
    azimuth['data'] = var[:]
    
    var = ncvars['Elevation']
    elevation = dict((k, getattr(var, k)) for k in var.ncattrs())
    elevation['data'] = var[:]

    fields = dict()

    var = ncvars['Zh']
    zh = dict((k, getattr(var, k)) for k in var.ncattrs())
    zh['data'] = var[:]
    fields['reflectivity_horizontal'] = zh
    
    var = ncvars['Zv']
    zv = dict((k, getattr(var, k)) for k in var.ncattrs())
    zv['data'] = var[:]
    fields['reflectivity_vertical'] = zv
    
    var = ncvars['Phidp']
    phidp = dict((k, getattr(var, k)) for k in var.ncattrs())
    phidp['data'] = var[:] + 5
    fields['differential_phase'] = phidp
    
    var = ncvars['RhoHV']
    rhohv = dict((k, getattr(var, k)) for k in var.ncattrs())
    rhohv['data'] = var[:] / 100.
    fields['cross_correlation_ratio'] = rhohv

    var = ncvars['VmeanH']
    velocity = dict((k, getattr(var, k)) for k in var.ncattrs())
    velocity['data'] = var[:]
    fields['velocity'] = velocity
    
    
    
    a = np.ndarray(zh['data'].shape)
    a.fill(1)
    normalized_coherent_power = dict()
    normalized_coherent_power['data'] = a
    fields['normalized_coherent_power'] = normalized_coherent_power
        
    
    ngates = len(zh['data'][0])
    nray = len(zh['data'][:,0])
    

    temp = dict()
    temp['data'] = np.ones(ngates) 
    fields['temperature'] = temp



    fixed_angle = dict()
    fixed_angle['data'] = [elevation['data'][0]]
    
    sweep_number = dict()
    sweep_number['data'] = np.array([0], dtype='int32')
    sweep_start_ray_index = dict()
    sweep_start_ray_index['data'] = np.array([0], dtype='int32')
    sweep_end_ray_index = dict()
    sweep_end_ray_index['data'] = np.array([nray-1], dtype='int32')
    
    metadata = None
    sweep_mode = None
    
    
    instrument_parameters = dict()
    frequency = dict()
    frequency['data'] = np.array([9400000000], dtype='float64')
    instrument_parameters['frequency'] = frequency

    return Radar(
        time, _range, fields, metadata, scan_type,
        latitude, longitude, altitude,
        sweep_number, sweep_mode, fixed_angle, sweep_start_ray_index,
        sweep_end_ray_index,
        azimuth, elevation,
        instrument_parameters=instrument_parameters)

