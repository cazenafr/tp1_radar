#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 11:37:49 2022

@author: davy
"""

import numpy as np
import pyart


def cor_phidp(radar, seuil_nval_phidp = 10):
    
    phidp_field = 'differential_phase'
    rhv_field = 'cross_correlation_ratio'
    
    
    phidp = radar.fields[phidp_field]['data']
    rhv = radar.fields[rhv_field]['data']
    
    
    phidp_cor = np.zeros(phidp.shape, dtype='float32')
    
    
    nsweeps = int(radar.nsweeps)
    
    for sweep in range(nsweeps):
        
        start_ray = radar.sweep_start_ray_index['data'][sweep]
        end_ray = radar.sweep_end_ray_index['data'][sweep] + 1
        
        for i in range(start_ray, end_ray):
            
            ray_rhv = rhv[i, :]
            ray_phidp = phidp[i,:]
            
            
            ideb = 6
            ifin = 0
            for j in range(len(ray_rhv)-10):
                if ray_rhv[j] < 0.95:
                    ideb = j+1
                elif (j-ideb >= 10) :
                        break
            for j in range(len(ray_rhv) - 1, 10, -1):
                if (ray_rhv[j] < 0.95):
                    ifin = j-1
                # elif ifin-j >= 10:
                else:
                    break
            
            if(ifin-ideb < 10):
                continue
            
            
            nport = len(ray_phidp)
            
            
            
            phidp0 = np.median(ray_phidp[ideb:ideb+5])
            
                        
            phidpraw = np.zeros(nport)
            phidpraw[ideb:ifin] = ray_phidp[ideb:ifin] - phidp0
            phidpraw = np.where(phidpraw < 0, 0, phidpraw)
            
            
            
            # print("az = ", radar.azimuth['data'][i])
            # print(nport, ideb, ifin)
            # print("phidp0 = ", phidp0)
            
            
            diffmax = np.arange(0.2,10,0.2)
            niter = len(diffmax)
            
            
            Nash = np.zeros(niter)
            vmin = np.zeros([nport, niter])
            vmax = np.zeros([nport, niter])
            vmoy = np.full([nport, niter], np.nan)
            
                            
            # Boucle sur iter
            #================
            for iter in range(niter):
                            
              
                # calcul de vmax
                #---------------
                vmax[1:(ideb-1),iter] = 0
                
                for ind1 in range(ideb+1,ifin):
                    diff = phidpraw[ind1] - vmax[ind1-1,iter]
                    if(diff>0 and diff<=diffmax[iter]): vmax[ind1,iter] = phidpraw[ind1]
                    elif (diff>diffmax[iter]) : vmax[ind1,iter] = vmax[(ind1-1),iter] + diffmax[iter]
                    else : vmax[ind1,iter] = vmax[(ind1-1),iter]
                    
                    if (np.isnan(phidpraw[ind1])):
                        vmax[ind1,iter] = vmax[(ind1-1),iter]
                
                
                # calcul de vmin
                #---------------
                # recherche de la premiere valeur positive en partant de la fin
                ind6 = np.nan
                for ind5 in range(ifin,ideb) :
                    if (phidpraw[ind5] > 0) and not np.isnan(phidpraw[ind5]):
                        ind6 = ind5
                        vmin[ind6,iter] = phidpraw[ind6]
                        break
                  
            
                # calcul vmoy si ind6 existe
                #---------------------------
                if(not np.isnan(ind6)):
                    for ind2 in range((ind6-1), ideb):
                        diff = vmin[(ind2+1),iter] - phidpraw[ind2]
                        if(diff > 0 & diff < diffmax[iter]) : vmin[ind2,iter] = phidpraw[ind2]
                        elif(diff>diffmax[iter]): vmin[ind2,iter] = vmin[(ind2+1),iter]-diffmax[iter]
                        else : vmin[ind2,iter] = vmin[(ind2+1),iter]
                        
                        if (np.isnan(phidpraw[ind2])) : vmin[ind2,iter] = vmin[(ind2+1),iter]
                        if (vmin[ind2,iter] > vmax[ind2,iter]) : vmin[ind2,iter] = vmax[ind2,iter]
                    
                    
                    if(ind6 < nport):
                        vmin[ind6:nport,iter] = np.nan
                        vmax[ind6:nport,iter] = np.nan
                  
                vmoy[:,iter] = (vmin[:,iter] + vmax[:,iter])
              
                # print(paste(" iter, vmin : ",iter,sep=""))
                # print(paste(as.numeric(vmin[1:100,iter])))    
                # print(paste(" iter, vmoy : ",iter,sep=""))
                # print(paste(as.numeric(vmoy[1:100,iter])))  
                # print(paste(" iter, vmax : ",iter,sep=""))
                # print(paste(as.numeric(vmax[1:100,iter])))  
              
              
                # calcul MAD entre phidpraw et vmoy
                #----------------------------------
                good = np.logical_and(np.logical_not(np.isnan(phidpraw)), np.logical_not(np.isnan(vmoy[:,iter])))
                
                                
                y = np.extract(good, vmoy[:,iter])
                x = np.extract(good, phidpraw)
                
                nval = len(x)
              
                if(nval >= seuil_nval_phidp):
                    mean_observed = sum(x) / nval
                    numerator = 0
                    denominator = 0                    
                    for j in range(nval):
                        numerator += (x[j] - y[j])*(x[j] - y[j])
                        denominator += (x[j] - mean_observed)*(x[j] - mean_observed)
                    Nash[iter] = 1-(numerator/denominator)
                else:
                    Nash[iter] = np.nan
                
                # print(paste(" iter, Nash[iter] : ",iter, Nash[iter],sep=" "))
                
                # fin de boucle sur iter
                
                NashnoNA = np.extract(np.logical_not(np.isnan(Nash)), Nash)
                
                # au moins un Nash non NA   
                if (len(NashnoNA) >= 1):
                
                    ibest = np.nanargmax(Nash)
                    
                    Nash_opt = Nash[ibest]
                    
                    # print(" ibest : ",ibest," Nash :",Nash_opt)
                    
                    # vmin_ret = vmin[:,ibest]
                    # vmax_ret = vmax[:,ibest]
                    # vmoy_ret = vmoy[:,ibest]
                    
                        
                    phidp_cor[i] = vmoy[:,ibest]
                    
                  
                # fin de if length
            
            
    cor_phi = dict()
    cor_phi['data'] = phidp_cor
            
    return cor_phi
    

    